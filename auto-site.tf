# Define the provider. In this case we're building in azure.

provider "azurerm" {
  # Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
  version = "=1.28.0"
}

resource "azurerm_resource_group" "estate" {
  name     = "${var.estate_name}-rg"
  location = "West Europe"
}

resource "azurerm_app_service_plan" "app1-sp" {
  name                = "${var.app1}-sp"
  location            = "${azurerm_resource_group.estate.location}"
  resource_group_name = "${azurerm_resource_group.estate.name}"
  kind                = "Linux"
  reserved            = true
  sku {
    tier = "Basic"
    size = "B1"
  }
}


resource "azurerm_app_service" "app1-as" {
    name = "auto-site"
    location = "${azurerm_resource_group.estate.location}"
    app_service_plan_id = "${azurerm_app_service_plan.app1-sp.id}"
    resource_group_name = "${azurerm_resource_group.estate.name}"

    site_config {
    dotnet_framework_version = "v4.0"
    scm_type                 = "ExternalGit"
	source_control {
      repo_url = "https://gitlab.com/acpatt/auto-site"
      branch = "master"
      }
    }

}
